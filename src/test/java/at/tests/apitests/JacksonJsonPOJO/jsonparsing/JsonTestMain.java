package at.tests.apitests.JacksonJsonPOJO.jsonparsing;

import at.api.JacksonJsonPOJO.jsonparsing.Json;
import com.fasterxml.jackson.databind.JsonNode;

import java.io.IOException;

/**
 * Этот класс парсит Объект jsonSource
 */
public class JsonTestMain {
    public static void main(String[] args) {
        //Объект jsonSource
        String jsonSource = "{\"title\" : \"Coder From Scratch\"}";

        try {
            //парсим Объект jsonSource
            JsonNode node = Json.parse(jsonSource);

            //Вывод в консоль node - результата распарсивания Объекта jsonSource
            System.out.println(node.get("title").asText());

        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
