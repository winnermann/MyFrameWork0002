package at.web.selenium.test_ng;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.testng.Assert;
import org.testng.annotations.*;

import java.util.concurrent.TimeUnit;

public class TestNGInclude {

    public String baseUrl = "http://www.edureka.co/";
    String driverPath = "src/test/resources/chromedriver.exe";
    public WebDriver driver;

    @BeforeSuite
    public void beforeSuite(){
        System.out.println("beforeSuite");
    }

    /**
     * @BeforeTest - эта аннотация testNG всегда сработает перед @Test
     * и стартанет драйвер (браузер)
     */
    @BeforeTest
    public void launchBrowser(){
        System.out.println("BeforeTest");
        System.out.println("launchBrowser");
        System.setProperty("webdriver.chrome.driver", driverPath);
        driver = new ChromeDriver();
        driver.manage().window().maximize();
        driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
        driver.get(baseUrl);

    }

    @BeforeClass
    public void beforeClass(){
        System.out.println("beforeClass");
    }

    @BeforeMethod
    public void beforeMethod(){
        System.out.println("beforeMethod");
    }

    /**
     * @Test - это аннотация testNG для тест-кейсов
     */
    @Test(description = "Cценарий: Compare Titles", priority = 1, timeOut = 60000 * 10)
    public void checkTitleEdureka(){
        System.out.println("checkTitleEdureka");
        String expectedTitle = "Instructor-Led Online Training with 24X7 Lifetime Support | Edureka";
        String actualTitle = driver.getTitle();
        System.out.println(actualTitle);
        Assert.assertEquals(actualTitle, expectedTitle);

    }

    @Test(description = "Cценарий: negativeLoginEdureka", priority = 2, timeOut = 60000 * 10)
    public void negativeLoginEdureka() throws InterruptedException {
        //нажать на кнопку "Log In"
        WebElement Login = driver.findElement(By.linkText("Log In"));
        Login.click();
        Thread.sleep(4000);
        //ввести валидное userName
        WebElement userName = driver.findElement(By.id("si_popup_email"));
        userName.sendKeys("omkar.hiremath@edureka.co");
        Thread.sleep(4000);
        //ввести невалидный password
        WebElement password = driver.findElement(By.id("si_popup_passwd"));
        password.sendKeys("12345678");
        Thread.sleep(6000);
        //нажать кнопку Next
        WebElement Next = driver.findElement(By.xpath("//button[@class='clik_btn_log btn-block']"));
        Next.click();
        Thread.sleep(10000);
        WebElement errorMessage = driver.findElement(By.id("passwdErrorr"));
        System.out.println(errorMessage.getText());
        String errorMessageExpected = "Sign In Failed. Invalid login credentials.";
        //убедиться, что отобразилось сообщение об ошибке с ожидаемым текстом
        Assert.assertEquals(errorMessage.getText(), errorMessageExpected);
        System.out.println("testCase2");
    }

    @Test(description = "Cценарий: testCase3", priority = 3, timeOut = 60000 * 10)
    public void testCase3(){
        System.out.println("testCase3");
    }

    @Test(description = "Cценарий: testCase4", priority = 4, timeOut = 60000 * 10)
    public void testCase4(){
        System.out.println("testCase4");
    }

    @Test(description = "Cценарий: testCase5", priority = 5, timeOut = 60000 * 10)
    public void testCase5(){
        System.out.println("testCase5");
    }

    @Test(description = "Cценарий: testCase6", priority = 6, timeOut = 60000 * 10)
    public void testCase6(){
        System.out.println("testCase6");
    }

    @Test(description = "Cценарий: testCase7", priority = 7, timeOut = 60000 * 10)
    public void testCase7(){
        System.out.println("testCase7");
    }

    @AfterMethod
    public void afterMethod(){
        System.out.println("afterMethod");
    }

    @AfterClass
    public void afterClass(){
        System.out.println("afterClass");
    }

    /**
     * @AfterTest - эта аннотация testNG всегда сработает после @Test
     * и даже если тест упал драйвер (браузер) будет закрыт
     */
    @AfterTest
    public void terminateBrowser(){
        System.out.println("afterTest");
        driver.close();
    }

    @AfterSuite
    public void afterSuite(){
        System.out.println("afterSuite");
    }
}
