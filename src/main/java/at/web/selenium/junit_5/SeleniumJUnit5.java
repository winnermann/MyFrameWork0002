package at.web.selenium.junit_5;

import io.qameta.allure.Description;
import org.junit.jupiter.api.*;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;

import java.util.concurrent.TimeUnit;

import static org.junit.jupiter.api.Assertions.*;


/**
 * Порядок срабатывания аннотаций JUnit5
 * @BeforeAll -срабатывает перед @BeforeEach, @Test
 * @BeforeEach - срабатывает перед каждым методом помеченным @Test
 *
 * @Test - запускает тесты в порядке приоритета
 *
 * @AfterEach - срабатывает после каждого метода помеченного @Test
 * @AfterAll - срабатывает после @AfterEach, @Test
 */

/**
 * @Test - запускает тесты в порядке приоритета
 *
 * @TestMethodOrder(MethodOrderer.OrderAnnotation.class) - Для управления приоритетом запуска тестов, ставится над классом
 * @Order(1) - ставится над методом помеченным @Test и этот метод будет запущен первым
 * @Order(2) - ставится над методом помеченным @Test и этот метод будет запущен вторым
 *
 * @Disabled - исключает метод помеченный @Test из прогона
 */

@TestMethodOrder(MethodOrderer.OrderAnnotation.class)
public class SeleniumJUnit5 {
    public static String baseUrl = "http://www.edureka.co/";
    static String driverPath = "src/test/resources/chromedriver.exe";
    public static WebDriver driver;

    @BeforeAll
    public static void beforeAll(){
        System.out.println("beforeAll");
        System.out.println("launchBrowser");
        System.setProperty("webdriver.chrome.driver", driverPath);
        driver = new ChromeDriver();
        driver.manage().window().maximize();
        driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
        driver.get(baseUrl);
    }

    @BeforeEach
    public void beforeEach(){
        System.out.println("beforeEach");
    }

    @Order(1)
    @Test
    @DisplayName("Cценарий: Compare Titles")
    public void checkTitleEdureka(){
        System.out.println("Первый тест JUNIT5");
        System.out.println("checkTitleEdureka");
        String expectedTitle = "Instructor-Led Online Training with 24X7 Lifetime Support | Edureka";
        String actualTitle = driver.getTitle();
        System.out.println(actualTitle);
        assertEquals(actualTitle, expectedTitle);
    }

    @Order(2)
    @Test
    @DisplayName("Cценарий: Попытка Авторизации")
    public void test2() throws InterruptedException {
        System.out.println("test2");
        //нажать на кнопку "Log In"
        WebElement Login = driver.findElement(By.linkText("Log In"));
        Login.click();
        Thread.sleep(4000);
        //ввести валидное userName
        WebElement userName = driver.findElement(By.id("si_popup_email"));
        userName.sendKeys("omkar.hiremath@edureka.co");
        Thread.sleep(4000);
        //ввести невалидный password
        WebElement password = driver.findElement(By.id("si_popup_passwd"));
        password.sendKeys("12345678");
        Thread.sleep(6000);
        //нажать кнопку Next
        WebElement Next = driver.findElement(By.xpath("//button[@class='clik_btn_log btn-block']"));
        Next.click();
        Thread.sleep(10000);
        WebElement errorMessage = driver.findElement(By.id("passwdErrorr"));
        System.out.println(errorMessage.getText());
        String errorMessageExpected = "Sign In Failed. Invalid login credentials.";
        //убедиться, что отобразилось сообщение об ошибке с ожидаемым текстом
        assertEquals(errorMessage.getText(), errorMessageExpected);
    }

    @Order(3)
    @Test
    @DisplayName("Тест3")
    public void test3(){
        int expected = 1;
        int actual = 1;
        assertEquals(expected, actual);
        System.out.println("test3");
    }

    @Order(4)
    @Test
    @DisplayName("Тест4")
    public void test4(){
        int expected = 1;
        int actual = 1;
        assertEquals(expected, actual);
        System.out.println("test4");
    }

    @Order(5)
    @Test
    @DisplayName("Тест5")
    public void test5(){
        int expected = 1;
        int actual = 1;
        assertEquals(expected, actual);
        System.out.println("test5");
    }

    @Order(6)
    @Disabled("Этот тест будет исключен из прогона")
    @Test
    @DisplayName("Тест6")
    public void test6(){
        int expected = 1;
        int actual = 1;
        assertEquals(expected, actual);
        System.out.println("test6");
    }

    @Order(7)
    @Test
    @DisplayName("Тест7")
    public void test7(){
        int expected = 1;
        int actual = 1;
        assertEquals(expected, actual);
        System.out.println("test7");
    }

    @AfterEach
    public void afterEach(){
        System.out.println("afterEach");
    }

    @AfterAll
    public static void afterAll(){
        System.out.println("afterAll");
        driver.close();
    }
}
