package at.web.selenium.junit_5;

import org.junit.jupiter.api.*;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;

import java.util.concurrent.TimeUnit;

import static org.junit.jupiter.api.Assertions.assertEquals;

@TestMethodOrder(MethodOrderer.OrderAnnotation.class)
public class CalendarMakemytrip {
    public static String baseUrl = "https://www.makemytrip.com/";
    static String driverPath = "src/test/resources/chromedriver.exe";
    public static WebDriver driver;

    @BeforeAll
    public static void openBrowser(){
        System.setProperty("webdriver.chrome.driver", driverPath);
        driver = new ChromeDriver();
        driver.manage().window().maximize();
        driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
        driver.get(baseUrl);
    }

    @Order(1)
    @Test
    @DisplayName("Cценарий: Выбрать дату не отображающуюся в календаре")
    public void checkTitleEdureka() throws InterruptedException {
        String expectedTitle = "MakeMyTrip - #1 Travel Website 50% OFF on Hotels, Flights & Holiday";
        String actualTitle = driver.getTitle();
        System.out.println(actualTitle);
        assertEquals(actualTitle, expectedTitle);
        Thread.sleep(10000);

        //нажать кнопку "Departure"
        WebElement btnDeparture = driver.findElement(By.xpath("//div[@class='fsw_inputBox dates inactiveWidget ']"));
        btnDeparture.click();

        String flag = "False";
        while (flag=="False"){
            //Если в календаре дата 'Fri Aug 20 2021' видимая, то нажать эту дату
            if (driver.findElements(By.xpath("//div[@class='DayPicker-Day'][@aria-label='Fri Aug 20 2021']")).size()>0){
                driver.findElement(By.xpath("//div[@class='DayPicker-Day'][@aria-label='Fri Aug 20 2021']")).click();
                flag="True";
                Thread.sleep(1000);
            }
            //Если в календаре дата 'Fri Aug 20 2021' не отображается, то нажать стрелочку "Перейти к следующему месяцу"
            else {
                Thread.sleep(1000);
                WebElement btnRightArrow = driver.findElement(By.xpath("//span[@aria-label='Next Month']"));
                btnRightArrow.click();
            }
        }
    }

    @AfterAll
    public static void closeBrowser(){
        driver.close();
    }
}
