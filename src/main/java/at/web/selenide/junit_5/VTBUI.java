package at.web.selenide.junit_5;

import com.codeborne.selenide.Configuration;
import com.codeborne.selenide.Selenide;
import io.qameta.allure.Step;
import org.openqa.selenium.By;

import static com.codeborne.selenide.Selenide.*;
import static org.junit.jupiter.api.Assertions.assertEquals;

public class VTBUI {
    public static void startBrowser(){
        //Открыть браузер
        String driverPath = "src/test/chromedriver.exe";
        System.setProperty("webdriver.chrome.driver", driverPath);
        System.setProperty("selenide.browser", "chrome");
        Configuration.driverManagerEnabled = false;
        Configuration.browser="chrome";
        Configuration.startMaximized = true;
        Configuration.timeout = 6000;
        open("hfgh");
    }

    @Step("Найти карточку ДЦ")
    public static void findDCCard(){
        System.out.println("findDCCard");

        String month = "Февраль 2021";

        //Навести на элемент заголовок колонки "Дата прогноза" и зависнуть до появления стрелочки
        $(By.xpath("//")).scrollTo().hover();

        //Сместиться с колонки "Дата прогноза" на элемент "Стрелочка с вападающим меню" и кликнуть стрелочку
        $(By.xpath("//")).scrollTo().hover().click();

        //Сместиться к меню "Фильтр" и зависнуть
        $(By.xpath("//")).scrollTo().hover();

        //Сместиться к меню "На дату" и кликнуть меню "На дату"
        $(By.xpath("//")).scrollTo().hover().click();


        /**
         * Этот алгоритм ищет нужную дату в календаре
         */
        while (true){
            //Считывает текст с плашки "месяц, год" и помещает в переменную text
            String text = $(By.xpath("//")).getText();
            //Если месяц на плашке (text) равен месяцу который ищем(month) ничего не делать break;
            if (text.equals(month)){
                break;
            }else {
                //иначе, кликнуть на стрелочку назад
                $(By.xpath("//")).click();
            }
        }

        //кликнуть дату 26.02.2021
        $(By.xpath("//")).click();

        //Кликнуть на элемент заголовок колонки "Дата прогноза", чтобы закрылось выпадающее меню
        $(By.xpath("//")).scrollTo().hover().click();

        //Убедиться что ФИО клиента отобразилось в результатах поиска
        String clientFioFromVM = element(By.xpath("")).getText();
        assertEquals("Master Masterovich", clientFioFromVM);

        //Убедиться что номер претензии отобразился в результатах поиска
        String claimNumberFromVM = element(By.xpath("")).getText();
        assertEquals("f3701ce5-4754-4d48968d-147145d81d9d", claimNumberFromVM);
    }
}
