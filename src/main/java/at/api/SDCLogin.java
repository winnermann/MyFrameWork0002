package at.api;

import io.qameta.allure.Step;
import io.restassured.config.RedirectConfig;
import io.restassured.filter.Filter;
import io.restassured.filter.cookie.CookieFilter;
import io.restassured.response.ExtractableResponse;
import io.restassured.response.Response;

import static io.restassured.RestAssured.*;

public class SDCLogin {

    private SDCLogin(){
        //Что бы не создавали
    }

    /**
     * Вход в систему с параметрами, указанными в конфиге
     */


    public static Filter login(){
        return login("sdc_terminal", Config.getSDCDefaultUsername(),
                Config.getSDCDefaultPassword());

    }

    /**
     * Вход с параметрами, указанными в конфиге
     */

    public static Filter login(String whereTo){
        return login(whereTo, Config.getSDCDefaultUsername(),
                Config.getSDCDefaultPassword());

    }

    /**
     * Зайти в Систему
     * Во время этого действия будет установлен JSessionID
     *
     * @param whereTo куда авторизация
     * @param username имя пользователя
     * @param password пароль
     */
    @Step("Вход в СДЦ для пользователя {0}")
    public static Filter login(String whereTo, String username, String password){
        baseURI = Config.getSDCHost();
        port = Config.getSDCPort();
        basePath = "";
        Filter filter = new CookieFilter();

        ExtractableResponse<Response> extract = expect().
                statusCode(303).
                given().
                filter(filter).
                config(config().redirect(RedirectConfig.redirectConfig().followRedirects(false))).
                param("j_username", username).
                param("j_password", password).
                post(whereTo + "/j_security_check")
                .then().extract();
        String location = extract.header("Location");
        //Проверим, что после редиректа нас не редиректит снова на логин.
        given()
                .filter(filter).config(config().redirect(RedirectConfig.redirectConfig().followRedirects(false)))
                .get(location)
                .then().statusCode(200)
                .extract()
                .sessionId();
        return filter;

    }
}
