package at.api;

import at.common.Config.CommonSpec;
import io.restassured.response.Response;
import org.junit.jupiter.api.*;

import java.io.File;
import static org.hamcrest.Matchers.containsString;

@TestMethodOrder(MethodOrderer.OrderAnnotation.class)
public class BR14790TC1ApiTest {
    private static final String SDC_TERMINAL_ATM_TERMINAL_UPLOAD_TERMINAL_DOCTIONARY = "/sdc_terminal/atm/task/list.json";

    @Order(1)
    @Test
    @DisplayName("Cценарий: Загрузить на сервер справочник терминалов")
    public void uploadTerminalDictionary(){
        System.out.println("uploadTerminalDictionary");

        File file = new File("upload/step1/Список УС на 04.01.2019.xlsx");

        Response response;
        response = CommonSpec.get("sdc_terminal").
                contentType("multipart/form-data").
                with().
                multiPart("file", file, "multipart/form-data").
                when().
                post(SDC_TERMINAL_ATM_TERMINAL_UPLOAD_TERMINAL_DOCTIONARY);

        response.then().
                statusCode(201).
                body(containsString("{\"message\":\"Справочник терминалов загружен\",\"msg\":\"Справочник терминалов загружен\",\"success\":true}")).
                log().all();

    }
}
